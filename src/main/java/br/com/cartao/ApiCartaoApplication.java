package br.com.cartao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiCartaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiCartaoApplication.class, args);
	}

}
