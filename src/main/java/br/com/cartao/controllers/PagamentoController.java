package br.com.cartao.controllers;

import br.com.cartao.models.Cliente;
import br.com.cartao.models.dtos.CartaoEntradaDTO;
import br.com.cartao.models.dtos.PagamentoEntradaDTO;
import br.com.cartao.models.dtos.PagamentoRespostaDTO;
import br.com.cartao.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;


@RestController
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoRespostaDTO realizarPagamento(@RequestBody @Valid PagamentoEntradaDTO pagamentoEntradaDTO){
        try{
            return pagamentoService.realizarPagamento(pagamentoEntradaDTO);
        }catch(RuntimeException exception){
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/pagamentos/{id_cartao}")
    public Iterable<PagamentoRespostaDTO> listarPagamentosPorCartao(@PathVariable int id_cartao){
        try{
            Iterable<PagamentoRespostaDTO> pagamentosRespostaDTO = pagamentoService.listarPagamentosPorCartao(id_cartao);
            return  pagamentosRespostaDTO;

        }catch(RuntimeException exception){
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
