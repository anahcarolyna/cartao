package br.com.cartao.controllers;

import br.com.cartao.models.Cartao;
import br.com.cartao.models.Cliente;
import br.com.cartao.models.dtos.CartaoAtivarEntradaDTO;
import br.com.cartao.models.dtos.CartaoEntradaDTO;
import br.com.cartao.models.dtos.CartaoRespostaDTO;
import br.com.cartao.models.dtos.CartaoRespostaGetDTO;
import br.com.cartao.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoRespostaDTO solicitarCartao(@RequestBody @Valid CartaoEntradaDTO cartaoEntradaDTO){
        try{
            return cartaoService.solicitarCartao(cartaoEntradaDTO);

        }catch(RuntimeException exception){
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PatchMapping("/{numero}")
    public CartaoRespostaDTO ativarCartao(@PathVariable(name = "numero") String numero, @RequestBody
            CartaoAtivarEntradaDTO cartaoAtivarEntradaDTO){
        try{
            return cartaoService.ativarCartao(numero, cartaoAtivarEntradaDTO);

        }catch(RuntimeException exception){
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/{numero}")
    public CartaoRespostaGetDTO buscarCartaoPorNumero(@PathVariable String numero){
        try{
            return cartaoService.buscarPorNumeroCartao(numero);

        }catch(RuntimeException exception){
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

}
