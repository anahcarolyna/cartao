package br.com.cartao.controllers;

import br.com.cartao.models.Cliente;
import br.com.cartao.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente incluirCliente(@RequestBody @Valid Cliente cliente){
        try{
            return clienteService.incluirCliente(cliente);

        }catch(RuntimeException exception){
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Cliente buscarPorID(@PathVariable int id){
        try{
            Cliente cliente = clienteService.buscarPorId(id);
            return  cliente;

        }catch(RuntimeException exception){
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
