package br.com.cartao.exceptions.errors;

public class ErrorObject {
    private String mensagemDeErro;
    private String valorRejeitado;

    public ErrorObject(String mensagemDeErro, String valorRejeitado) {
        this.mensagemDeErro = mensagemDeErro;
        this.valorRejeitado = valorRejeitado;
    }

    public String getMensagemDeErro() {
        return mensagemDeErro;
    }

    public void setMensagemDeErro(String mensagemDeErro) {
        this.mensagemDeErro = mensagemDeErro;
    }

    public String getValorRejeitado() {
        return valorRejeitado;
    }

    public void setValorRejeitado(String valorRejeitado) {
        this.valorRejeitado = valorRejeitado;
    }
}
