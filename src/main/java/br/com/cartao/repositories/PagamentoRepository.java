package br.com.cartao.repositories;

import br.com.cartao.models.Cartao;
import br.com.cartao.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    Iterable<Pagamento> findAllByCartao(Cartao cartao);
}
