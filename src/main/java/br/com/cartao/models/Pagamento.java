package br.com.cartao.models;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Pagamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min = 5, max = 100, message = "A descrição deve ter entre 5 a  100 caracteres")
    @NotNull(message = "É obrigatório o preenchimento da descrição")
    private String descricao;

    @Digits(integer = 10, fraction = 2, message = "O valor está no formato incorreto")
    @NotNull(message = "É obrigatório o preenchimento do valor")
    private double valor;

    @ManyToOne(cascade = CascadeType.ALL)
    @NotNull(message = "É obrigatório o preenchimento da cartão")
    private Cartao cartao;

    public Pagamento() {
    }

    public Pagamento(int id, @Size(min = 5, max = 100, message = "A descrição deve ter entre 5 a  100 caracteres") @NotNull(message = "É obrigatório o preenchimento da descrição") String descricao, @Digits(integer = 10, fraction = 2, message = "O valor está no formato incorreto") double valor, Cartao cartao) {
        this.id = id;
        this.descricao = descricao;
        this.valor = valor;
        this.cartao = cartao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Cartao getCartao() {
        return cartao;
    }

    public void setCartao_id(Cartao cartao) {
        this.cartao = cartao;
    }
}
