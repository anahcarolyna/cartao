package br.com.cartao.models.dtos;

import br.com.cartao.models.Cliente;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CartaoEntradaDTO {

    @Column(unique = true)
    @Size(min = 9, max = 16, message = "O número do cartão deve ter entre 9 a 16 caracteres")
    @NotNull(message = "É obrigatório o preenchimento do número do cartão")
    private String numero;

    private int clienteId;

    public CartaoEntradaDTO(@Size(min = 9, max = 16, message = "O número do cartão deve ter entre 9 a 16 caracteres") @NotNull(message = "É obrigatório o preenchimento do número do cartão") String numero, int clienteId) {
        this.numero = numero;
        this.clienteId = clienteId;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }
}
