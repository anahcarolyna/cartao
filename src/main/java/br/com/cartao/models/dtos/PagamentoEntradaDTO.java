package br.com.cartao.models.dtos;

import br.com.cartao.models.Pagamento;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PagamentoEntradaDTO {

    @NotNull(message = "É obrigatório o preenchimento da cartão")
    private int cartao_id;

    @Size(min = 5, max = 100, message = "A descrição deve ter entre 5 a  100 caracteres")
    @NotNull(message = "É obrigatório o preenchimento da descrição")
    private  String descricao;

    @Digits(integer = 10, fraction = 2, message = "O valor está no formato incorreto")
    @NotNull(message = "É obrigatório o preenchimento do valor")
    private double valor;

    public PagamentoEntradaDTO() {
    }

    public PagamentoEntradaDTO(Pagamento pagamento) {
        this.cartao_id = pagamento.getCartao().getId();
        this.descricao = pagamento.getDescricao();
        this.valor = pagamento.getValor();
    }

    public int getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(int cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
