package br.com.cartao.models.dtos;

import br.com.cartao.models.Cartao;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class CartaoRespostaDTO {

    private int id;
    private String numero;
    private int clienteID;
    private boolean ativo;

    public CartaoRespostaDTO(Cartao cartao) {
        this.id = cartao.getId();
        this.numero = cartao.getNumero();
        this.clienteID = cartao.getClienteId().getId();
        this.ativo = cartao.isAtivo();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteID() {
        return clienteID;
    }

    public void setClienteID(int clienteID) {
        this.clienteID = clienteID;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
