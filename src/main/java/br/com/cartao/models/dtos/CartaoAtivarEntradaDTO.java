package br.com.cartao.models.dtos;

public class CartaoAtivarEntradaDTO {
    Boolean ativo;

    public CartaoAtivarEntradaDTO() {
    }

    public CartaoAtivarEntradaDTO(Boolean ativo) {
        this.ativo = ativo;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
