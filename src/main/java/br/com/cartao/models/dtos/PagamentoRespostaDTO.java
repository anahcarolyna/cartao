package br.com.cartao.models.dtos;

import br.com.cartao.models.Pagamento;

public class PagamentoRespostaDTO {
    private int id;
    private int cartao_id;
    private  String descricao;
    private double valor;

    public PagamentoRespostaDTO() {
    }

    public PagamentoRespostaDTO(Pagamento pagamento) {
       this.id = pagamento.getId();
        this.cartao_id = pagamento.getCartao().getId();
        this.descricao = pagamento.getDescricao();
        this.valor = pagamento.getValor();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(int cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
