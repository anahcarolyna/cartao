package br.com.cartao.models.dtos;

import br.com.cartao.models.Cartao;

public class CartaoRespostaGetDTO {

    private int id;
    private String numero;
    private int clienteID;

    public CartaoRespostaGetDTO(Cartao cartao) {
        this.id = cartao.getId();
        this.numero = cartao.getNumero();
        this.clienteID = cartao.getClienteId().getId();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteID() {
        return clienteID;
    }

    public void setClienteID(int clienteID) {
        this.clienteID = clienteID;
    }

}
