package br.com.cartao.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Cartao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    @Size(min = 9, max = 16, message = "O número do cartão deve ter entre 9 a 16 caracteres")
    @NotNull(message = "É obrigatório o preenchimento do número do cartão")
    private String numero;

    @ManyToOne(cascade = CascadeType.ALL)
    private Cliente clienteId;

    @Type(type="true_false")
    private boolean ativo;

    public Cartao() {
    }

    public Cartao(int id, @Size(min = 9, max = 16, message = "O número do cartão deve ter entre 9 a 16 caracteres") @NotNull(message = "É obrigatório o preenchimento do número do cartão") String numero, Cliente clienteId, boolean ativo) {
        this.id = id;
        this.numero = numero;
        this.clienteId = clienteId;
        this.ativo = ativo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Cliente getClienteId() {
        return clienteId;
    }

    public void setClienteId(Cliente clienteId) {
        this.clienteId = clienteId;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }


}
