package br.com.cartao.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min = 5, max = 100, message = "O nome deve ter entre 5 a  100 caracteres")
    @NotNull(message = "É obrigatório o preenchimento do nome")
    @Column(unique = true)
    private String name;

    public Cliente() {

    }

    public Cliente(int id, @Size(min = 5, max = 100, message = "O nome deve ter entre 5 a  100 caracteres") @NotNull(message = "É obrigatório o preenchimento do nome") String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
