package br.com.cartao.service;

import br.com.cartao.models.Cartao;
import br.com.cartao.models.Cliente;
import br.com.cartao.models.dtos.CartaoAtivarEntradaDTO;
import br.com.cartao.models.dtos.CartaoEntradaDTO;
import br.com.cartao.models.dtos.CartaoRespostaDTO;
import br.com.cartao.models.dtos.CartaoRespostaGetDTO;
import br.com.cartao.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteService clienteService;

    public CartaoRespostaDTO solicitarCartao(CartaoEntradaDTO cartaoEntradaDTO){
        Cliente cliente = clienteService.buscarPorId(cartaoEntradaDTO.getClienteId());

        Cartao cartaoObjeto = new Cartao();

        if(cliente != null){
            cartaoObjeto.setNumero(cartaoEntradaDTO.getNumero());
            cartaoObjeto.setClienteId(cliente);
            cartaoObjeto.setAtivo(false);
            cartaoObjeto = cartaoRepository.save(cartaoObjeto);
        }

        CartaoRespostaDTO cartaoRespostaDTO = new CartaoRespostaDTO(cartaoObjeto);

        return cartaoRespostaDTO;
    }

    public Cartao buscarPorID(int id) {
        Optional<Cartao> optionalCartao = cartaoRepository.findById(id);
        if (optionalCartao.isPresent()) {
            return optionalCartao.get();
        }
        throw new RuntimeException("Cartão inexistente.");
    }

    public Cartao buscarPorNumero(String numero) {
        Optional<Cartao> optionalCartao = cartaoRepository.findByNumero(numero);
        if (optionalCartao.isPresent()) {
            return optionalCartao.get();
        }
        throw new RuntimeException("Cartão inexistente.");
    }

    public CartaoRespostaGetDTO buscarPorNumeroCartao(String numero) {
        Optional<Cartao> optionalCartao = cartaoRepository.findByNumero(numero);

        if (optionalCartao.isPresent()) {
            CartaoRespostaGetDTO cartaoRespostaGetDTO = new CartaoRespostaGetDTO(optionalCartao.get());

            return cartaoRespostaGetDTO;
        }
        throw new RuntimeException("Cartão inexistente.");
    }

    public CartaoRespostaDTO ativarCartao(String numero, CartaoAtivarEntradaDTO cartaoAtivarEntradaDTO){

        Cartao cartaoObjeto = buscarPorNumero(numero);

        if(cartaoObjeto != null){

            cartaoObjeto.setAtivo(cartaoAtivarEntradaDTO.getAtivo());
            cartaoObjeto = cartaoRepository.save(cartaoObjeto);

            CartaoRespostaDTO cartaoRespostaDTO = new CartaoRespostaDTO(cartaoObjeto);

            return cartaoRespostaDTO;
        }

        throw new RuntimeException("Cartão inexistente.");
    }

}
