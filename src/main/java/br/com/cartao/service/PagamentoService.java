package br.com.cartao.service;

import br.com.cartao.models.Cartao;
import br.com.cartao.models.Pagamento;
import br.com.cartao.models.dtos.CartaoEntradaDTO;
import br.com.cartao.models.dtos.PagamentoEntradaDTO;
import br.com.cartao.models.dtos.PagamentoRespostaDTO;
import br.com.cartao.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoService cartaoService;

    public PagamentoRespostaDTO realizarPagamento(PagamentoEntradaDTO pagamentoEntradaDTO){
        Cartao cartao = cartaoService.buscarPorID(pagamentoEntradaDTO.getCartao_id());

        if(cartao != null){
            Pagamento pagamento = new Pagamento();
            pagamento.setCartao_id(cartao);
            pagamento.setDescricao(pagamentoEntradaDTO.getDescricao());
            pagamento.setValor(pagamentoEntradaDTO.getValor());

            pagamento = pagamentoRepository.save(pagamento);

            PagamentoRespostaDTO pagamentoRespostaDTO = new PagamentoRespostaDTO(pagamento);

            return pagamentoRespostaDTO;
        }

        throw new RuntimeException("Cartão inválido.");
    }

    public Iterable<PagamentoRespostaDTO> listarPagamentosPorCartao(int id){
       Cartao cartao = cartaoService.buscarPorID(id);

        Iterable<Pagamento> pagamentos = pagamentoRepository.findAllByCartao(cartao);

        List<PagamentoRespostaDTO> pagamentoRespostaDTOS = new ArrayList<>();
        for (Pagamento pagamento: pagamentos
             ) {
            PagamentoRespostaDTO pagamentoRespostaDTO = new PagamentoRespostaDTO(pagamento);
            pagamentoRespostaDTOS.add(pagamentoRespostaDTO);
        }

        return pagamentoRespostaDTOS;
    }
}
