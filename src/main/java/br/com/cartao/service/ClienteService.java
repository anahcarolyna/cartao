package br.com.cartao.service;

import br.com.cartao.models.Cliente;
import br.com.cartao.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente incluirCliente(Cliente cliente){
        Cliente clienteObjeto =clienteRepository.save(cliente);

        return clienteObjeto;
    }

    public Cliente buscarPorId(int id){
        Optional<Cliente> optionalCliente = clienteRepository.findById(id);

        if(optionalCliente.isPresent()){
            return optionalCliente.get();
        }

        throw new RuntimeException("Cliente não cadastrado");
    }
}
